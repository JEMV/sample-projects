//
//  Pokemon.swift
//  PokeRadar
//
//  Created by Jose Enrique Montañez Villanueva on 4/28/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class Pokemon: NSObject {

    var id: Int
    var name: String
    var image: UIImage
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
        self.image = UIImage(named: "\(self.id).png")!
    }
    
    
    
}
