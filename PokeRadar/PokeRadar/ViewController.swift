//
//  ViewController.swift
//  PokeRadar
//
//  Created by Jose Enrique Montañez Villanueva on 4/28/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase
import GeoFire



class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    
    var isCentered = false
    
    var geoFire: GeoFire!
    var geoFireRef: DatabaseReference!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.reportPokemon(_:)), name: .pokemonSelected, object: nil)
        
        self.geoFireRef = Database.database().reference()
        
        self.geoFire = GeoFire(firebaseRef: self.geoFireRef)

        
        self.mapView.delegate = self
        self.mapView.userTrackingMode = .follow
        
        
        self.locationManager.delegate = self
        
        self.showSightingsOnMap(on: CLLocation(latitude: self.mapView.centerCoordinate.latitude, longitude: self.mapView.centerCoordinate.longitude))
        
        self.locationAuthStatus()
    }

    fileprivate func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.mapView.showsUserLocation = true
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    fileprivate func createSighting(for location: CLLocation, with pokemonID: Int) {
        self.geoFire.setLocation(location, forKey: "\(pokemonID)")
    }
    
    func showSightingsOnMap(on location: CLLocation) {
        let query = self.geoFire.query(at: location, withRadius: 2.0)
        query.observe(.keyEntered) { (key, location) in
            let annotation = PokemonAnnotation(coordinate: location.coordinate, pokemonID: Int(key)!)
            self.mapView.addAnnotation(annotation)
            
        }
    }
    
    
    // MARK: - Location Delegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.showsUserLocation = true
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        
        if !isCentered {
            if let location = userLocation.location {
                self.centerMap(on: location)
                self.isCentered = true
            }
        }
    }
    
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        let location = CLLocation(latitude: self.mapView.centerCoordinate.latitude, longitude: self.mapView.centerCoordinate.longitude)
        self.showSightingsOnMap(on: location)
        
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView : MKAnnotationView?
        let annotationIdentifier = "Pokemon"
        
        if annotation is MKUserLocation {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "User")
            annotationView?.image = #imageLiteral(resourceName: "character")
        } else if let dequeuedAnnotation = self.mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotation
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
        }
        
        if let annotationView = annotationView, let pokemonAnnotation = annotation as? PokemonAnnotation {
            annotationView.canShowCallout = true
            annotationView.image = pokemonAnnotation.pokemon.image
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            button.setImage(#imageLiteral(resourceName: "location-map-flat"), for: .normal)
            annotationView.rightCalloutAccessoryView = button
            
            
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if let annotation = view.annotation as? PokemonAnnotation {
            let place = MKPlacemark(coordinate: annotation.coordinate)
            let destination = MKMapItem(placemark: place)
            destination.name = "\(annotation.pokemon.name) Avistado"
            
            let distance = CLLocationDistance(1000)
            let span = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: distance, longitudinalMeters: distance)
            
            let options: [String: Any] = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: span.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: span.span), MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
            
            MKMapItem.openMaps(with: [destination], launchOptions: options)
            
        }
        
    }
    
    fileprivate func centerMap(on Location: CLLocation) {
        let region = MKCoordinateRegion(center: Location.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        
        self.mapView.setRegion(region, animated: true)
        
        
    }
    
    
    
    @objc func reportPokemon(_ notification: Notification) {
        
        if let pokemon = notification.object as? Pokemon {
            let location = CLLocation(latitude: self.mapView.centerCoordinate.latitude, longitude: self.mapView.centerCoordinate.longitude)
            self.createSighting(for: location, with: pokemon.id)
        }
    }
    
}

