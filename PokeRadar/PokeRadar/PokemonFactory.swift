//
//  PokemonFactory.swift
//  PokeRadar
//
//  Created by Jose Enrique Montañez Villanueva on 4/28/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class PokemonFactory {
    
    static let shared = PokemonFactory()
    private var pokemons: [Pokemon] = [Pokemon]()
    
    
    
    init() {
        let data = self.readDataFromCSV(filename: "pokemon", fileType: "csv")
        let csvRows = self.csv(data: data!)
        
        for row in csvRows {
            if let id = Int(row[0]) {
                if id > 151 { break }
                let name = row[1].capitalized
                let pokemon = Pokemon(id: id, name: name)
                self.pokemons.append(pokemon)
            }
        }
        
    }
    
    func getPokemon(with pokemonID: Int) -> Pokemon {
        return self.pokemons[pokemonID-1]
    }
    
    func getPokemonsCount() -> Int {
        return self.pokemons.count
    }
    
    private func csv(data: String) -> [[String]] {
        var result: [[String]] = []
        let rows = data.components(separatedBy: "\n")
        
        for row in rows {
            let columns = row.components(separatedBy: ",")
            result.append(columns)
        }
        
        return result
    }
    
    private func readDataFromCSV(filename: String, fileType: String) -> String! {
        guard let filepath = Bundle.main.path(forResource: filename, ofType: fileType) else { return nil}
        
        do {
            let contents = try String(contentsOfFile: filepath, encoding: .utf8)
            return contents
        } catch {
            print("ha habido un error procesando el fichero \(filename)")
            return nil
        }
    }
}
