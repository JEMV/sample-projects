//
//  PlayerView.swift
//  Name That Tune!
//
//  Created by Jose Enrique Montañez Villanueva on 07/07/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class PlayerView: UIView {
    
    

    weak var controller: GameViewController?
    var picker = UIPickerView()
    var selectButton = UIButton(type: .custom)
    var sortedSongs = [Song]()
    
    init(color: UIColor, songs: [Song], controller: GameViewController) {
        super.init(frame: .zero)
        
        self.controller = controller
        self.selectButton.backgroundColor = color
        self.sortedSongs = songs.sorted()
        
        self.backgroundColor = color
        
        self.picker.backgroundColor = .white
        self.picker.translatesAutoresizingMaskIntoConstraints = false
        self.selectButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.picker)
        self.addSubview(self.selectButton)
        
        self.picker.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.picker.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.picker.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.picker.bottomAnchor.constraint(equalTo: self.selectButton.topAnchor).isActive = true
        
        self.selectButton.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.selectButton.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.selectButton.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.selectButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        self.selectButton.setTitle("Seleccionar canción", for: .normal)
        self.selectButton.setTitleColor(.white, for: .normal)
        self.selectButton.showsTouchWhenHighlighted = true
        self.selectButton.addTarget(self, action: #selector(self.buttonTapped(_:)), for: .touchUpInside)
        
        self.picker.dataSource = self
        self.picker.delegate = self
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        let selectedSong = sortedSongs[self.picker.selectedRow(inComponent: 0)]
        self.controller?.selectSong(player: self.backgroundColor!, playerAnswer: selectedSong)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
//        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension PlayerView: UIPickerViewDelegate, UIPickerViewDataSource  {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.sortedSongs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.sortedSongs[row].attributes.name
    }
    
}
