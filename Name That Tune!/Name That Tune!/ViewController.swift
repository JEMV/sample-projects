//
//  ViewController.swift
//  Name That Tune!
//
//  Created by Jose Enrique Montañez Villanueva on 6/9/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit
import GameplayKit
import StoreKit

class ViewController: UIViewController {

    let devToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldXNlEzTlhVRkIifQ.eyJpc3MiOiJNNUw4UEZCMjhXIiwiaWF0IjoxNTYyNTE1OTM3LCJleHAiOjE1NjI1NTkxMzd9.HZMWfL2YiVFaay9dQsJiX2ad9xPr7N_Mkh6K5ebL3pjN7KSZvGhIs0uwdCxE1y3bGnBY2_oBtiuqZFT_ZFutag"
    let urlSession = URLSession(configuration: .default)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let playButton = UIButton(type: .system)
        self.view.addSubview(playButton)
        
        playButton.translatesAutoresizingMaskIntoConstraints = false
        playButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        playButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: 90).isActive = true
        playButton.setTitle("Empezar juego", for: .normal)
        playButton.addTarget(self, action: #selector(self.startGame), for: .touchUpInside)
        
    }

    @objc func startGame() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        switch SKCloudServiceController.authorizationStatus() {
        case .notDetermined:
            SKCloudServiceController.requestAuthorization { [weak self] autorizationStatus in
                DispatchQueue.main.async {
                    self?.startGame()
                }
            }
        case .authorized:
            self.requestCapabilities()
        default:
            self.showNoGameMessage("No se tienen los permisos necesarios para usar Apple Music")
        }
    }
    
    func requestCapabilities() {
        let serviceController = SKCloudServiceController()
        serviceController.requestCapabilities {[weak self] (capabilities, error) in
            DispatchQueue.main.async {
                if let error = error {
                    self?.showNoGameMessage(error.localizedDescription)
                    return
                }
                if capabilities.contains(.musicCatalogPlayback) {
                    // Podemos reproducir musica
                    serviceController.requestStorefrontCountryCode(completionHandler: { (countryCode, error) in
                        if let countryCode = countryCode {
                            self?.fetchSongs(fromCountry: countryCode)
                        } else {
                            self?.showNoGameMessage("Imposible determinar el pais en el que estás")
                        }
                    })
                } else if capabilities.contains(.musicCatalogSubscriptionEligible) {
                    // Nos podemos suscribir (gratis) para jugar
                    let subscribeController = SKCloudServiceSetupViewController()
                    let options: [SKCloudServiceSetupOptionsKey: Any] = [.action: SKCloudServiceSetupAction.subscribe,
                                                                         .messageIdentifier: SKCloudServiceSetupMessageIdentifier.playMusic]
                    subscribeController.load(options: options, completionHandler: { (success, error) in
                        if success {
                            self?.present(subscribeController, animated: true)
                        } else {
                            self?.showNoGameMessage(error?.localizedDescription ?? "Error desconocido")
                        }
                    })
                } else {
                    self?.showNoGameMessage("No se puede jugar debido a que no se puede suscribir a Apple Music")
                }
            }
        }
    }
    
    func showNoGameMessage(_ message: String) {
        let alertController = UIAlertController(title: "No se puede jugar", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertController, animated: true)
    }
    
    func fetchSongs(fromCountry countryCode: String) {
        var urlRequest = URLRequest(url: URL(string: "https://api.music.apple.com/v1/catalog/\(countryCode)/charts?types=songs")!)
        urlRequest.addValue("Bearer \(devToken)", forHTTPHeaderField: "Authorization")
        
        urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            guard let data = data else { return }
            DispatchQueue.main.async {
                var musicResult: MusicResult
                do {
                    let decoder = JSONDecoder()
                    musicResult = try decoder.decode(MusicResult.self, from: data)
                    if let songs = musicResult.results.songs.first?.data {
                        let shuffleSongs = songs.shuffled()
                        // Mostrar el viewController del juego
                        let gameVC = GameViewController()
                        gameVC.songs = shuffleSongs
                        gameVC.modalPresentationStyle = .fullScreen
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self?.present(gameVC, animated: true)
                        return
                    }
                    
                } catch {
                    print(error.localizedDescription)
                }
                self?.showNoGameMessage("No se puede recibir información del servidor de Apple Music")
            }
        }.resume()
    }
    
}

