//
//  MusicDataTypes.swift
//  Name That Tune!
//
//  Created by Jose Enrique Montañez Villanueva on 06/07/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import Foundation


struct MusicResult: Codable {
    var results: ResultList
}

struct ResultList: Codable {
    var songs: [Result]
}

struct Result: Codable {
    var name: String
    var chart: String
    var data: [Song]
}

struct Song: Codable, Comparable {
    var id: String
    var href: String
    var attributes: SongAttributes
    
    
    static func <(lhs: Song, rhs: Song) -> Bool {
        return lhs.attributes.name < rhs.attributes.name
    }
    
    static func ==(lhs: Song, rhs: Song) -> Bool {
        return lhs.attributes.name == rhs.attributes.name
    }
    
    
}

struct SongAttributes: Codable {
    var name: String
    var artistName: String
}
