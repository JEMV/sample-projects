//
//  GameViewController.swift
//  Name That Tune!
//
//  Created by Jose Enrique Montañez Villanueva on 07/07/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit
import MediaPlayer

class GameViewController: UIViewController {

    var songs = [Song]()
    var currentSong: Song!
    
    var player1: PlayerView!
    var player2: PlayerView!
    
    let scoreLabel1 = UILabel()
    let scoreLabel2 = UILabel()
    
    let musicPlayerController = MPMusicPlayerController.systemMusicPlayer
    
    var score1 = 0 {
        didSet {
            scoreLabel1.text = "ROJO: \(score1)"
        }
    }
    
    var score2 = 0 {
        didSet {
            scoreLabel2.text = "AZUL: \(score2)"
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.configurePlayerViews()
        self.configureScoreLabels()
        self.playNextSong()
    }
    
    
    func configurePlayerViews() {
        self.player1 = PlayerView(color: .red, songs: self.songs, controller: self)
        self.player2 = PlayerView(color: .blue, songs: self.songs, controller: self)

        for case let playerView? in [player1, player2] {
            playerView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(playerView)
            
            playerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
            playerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            playerView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5, constant: -25).isActive = true
            
        }
        
        player1.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        player2.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        player1.transform = CGAffineTransform(rotationAngle: .pi)
        
        
        
    }
    
    func configureScoreLabels() {
        for score in [self.scoreLabel1, self.scoreLabel2] {
            score.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(score)
            
            score.textAlignment = .center
            score.textColor = .white
            score.font = UIFont.boldSystemFont(ofSize: 20)
            score.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            
            score.heightAnchor.constraint(equalToConstant: 50).isActive = true
            score.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5).isActive = true
        }
        self.scoreLabel1.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.scoreLabel2.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        scoreLabel1.backgroundColor = .red
        scoreLabel2.backgroundColor = .blue
        
        scoreLabel1.transform = CGAffineTransform(rotationAngle: .pi)
        
        self.score1 = 0
        self.score2 = 0
    }

    func playNextSong() {
        if let song = songs.popLast() {
            self.currentSong = song
            let descriptor = MPMusicPlayerStoreQueueDescriptor(storeIDs: [song.id])
            self.musicPlayerController.setQueue(with: descriptor)
            self.musicPlayerController.play()
        } else {
            self.musicPlayerController.stop()
            let alertController = UIAlertController(title: "Fin del juego", message: "Rojo: \(self.score1)\nAzul: \(self.score2)", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func selectSong(player: UIColor, playerAnswer: Song) {
        if playerAnswer == currentSong {
            if player == .red {
                score1 += 1
            } else {
                score2 += 1
            }
            self.playNextSong()
        }
        
    }
    
}
