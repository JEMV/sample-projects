//
//  ViewController.swift
//  PokerFace
//
//  Created by Jose Enrique Montañez Villanueva on 5/5/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import Vision
import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var blurSlider: UISlider!
    
    var showSquares = true
    var inputImage: UIImage?
    
    var detectedFaces = [(observation: VNFaceObservation, isBlurred: Bool)]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(self.importImage))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.sharePhoto))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.toggleSquares(_:)))
        tapGesture.numberOfTapsRequired = 2
        self.imageView.addGestureRecognizer(tapGesture)
    }

    @objc func toggleSquares(_ sender: UITapGestureRecognizer) {
        self.showSquares = !self.showSquares
        self.addFaceRects()
    }
    
    override func viewDidLayoutSubviews() {
            self.addFaceRects()
    }
    
    
    @objc func importImage() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imagePicked = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        self.inputImage = imagePicked
        self.imageView.image = self.inputImage
        
        dismiss(animated: true) {
            // Detectar la cara de los usuarios
            self.detectFaces()
        }
        
    }
    
    func detectFaces() {
        
        guard let ciImage = CIImage(image: self.inputImage!) else { return }
        
        
        let request = VNDetectFaceRectanglesRequest { (request, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                
                guard let observations = request.results as? [VNFaceObservation] else { return }
                
                self.detectedFaces = Array(zip(observations, [Bool](repeating: false, count: observations.count)))
                DispatchQueue.main.async {
                    self.addFaceRects()
                }
                
            }
        }
        
        let handler = VNImageRequestHandler(ciImage: ciImage)
        
        do {
            try handler.perform([request])
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
    func addFaceRects() {
        // Elimina rectangulos anteriores
        
        self.imageView.subviews.forEach({$0.removeFromSuperview()})
        
        // Define el rectangulo de la imagen dentro de la ImageView
        let imageRect = self.imageView.contentClippingRect
        
        for (index, face) in self.detectedFaces.enumerated() {
            // Las fronteras de la cara
            let boundingBox = face.observation.boundingBox
            
            // Tamaño de la cara
            let size = CGSize(width: boundingBox.width * imageRect.width, height: boundingBox.height * imageRect.height)
            
            // Posicion de la cara
            var origin = CGPoint(x: imageRect.width * boundingBox.minX, y: (1-boundingBox.minY) * imageRect.height - size.height)
            
            // Offset
            origin.y += imageRect.minY
            
            // Colocamos la UIView
            let view = UIView(frame: CGRect(origin: origin, size: size))
            view.tag = index
            view.layer.borderColor = UIColor.red.cgColor
            if self.showSquares {
                view.layer.borderWidth = 1.5
            } else {
                view.layer.borderWidth = 0
            }
            
            
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.faceTapped(_:))))
            
            self.imageView.addSubview(view)
            
        }
    }
    
    func renderBlurredFaces() {
        
        guard let ciImage = CIImage(image: self.inputImage!) else {return}
        
        let filter = CIFilter(name: "CIPixellate")
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        filter?.setValue(Int(self.blurSlider.value), forKey: kCIInputScaleKey)
        guard let outputImage = filter?.outputImage else { return }
        
        let blurredImage = UIImage(ciImage: outputImage)
        
        let renderer = UIGraphicsImageRenderer(size: self.inputImage!.size)
        
        let result = renderer.image { (ctx) in
            self.inputImage!.draw(at: .zero)
            
            let path = UIBezierPath()
            
            for face in detectedFaces {
                if face.isBlurred {
                    let boundingBox = face.observation.boundingBox
                    let size = CGSize(width: boundingBox.width * self.inputImage!.size.width, height: boundingBox.height * self.inputImage!.size.height)
                    let origin = CGPoint(x: boundingBox.minX * self.inputImage!.size.width, y: (1 - boundingBox.minY) * self.inputImage!.size.height - size.height)
                    let rect = CGRect(origin: origin, size: size)
                    path.append(UIBezierPath(ovalIn: rect))
                    
                }
            }
            if !path.isEmpty {
                path.addClip()
                blurredImage.draw(at: .zero)
            }
        }
        self.imageView.image = result
    }
    
    @objc func faceTapped(_ sender: UITapGestureRecognizer) {
        guard let view = sender.view else { return }
        let tag = view.tag
        self.detectedFaces[tag].isBlurred = !self.detectedFaces[tag].isBlurred
        self.renderBlurredFaces()
    }
    
    @objc func sharePhoto() {
        guard let image = self.imageView.image else { return }
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(activityVC, animated: true, completion: nil) 
    }
    
    
    @IBAction func blurChanged(_ sender: UISlider) {
        self.renderBlurredFaces()
    }
}

