//
//  UIImageView+ContentClippingRect.swift
//  PokerFace
//
//  Created by Jose Enrique Montañez Villanueva on 5/8/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit



extension UIImageView {
    
    
    
    var contentClippingRect: CGRect {
        guard let image = self.image, contentMode == .scaleAspectFit else { return self.bounds}
        
        let imageWidth = image.size.width
        let imageHeigt = image.size.height
        
        guard imageWidth > 0 && imageHeigt > 0 else { return self.bounds}
        
        let scale: CGFloat
        if imageWidth > imageHeigt {
            scale = bounds.size.width / imageWidth
        } else {
            scale = bounds.size.height / imageHeigt
        }
        
        let clippingSize = CGSize(width: imageWidth * scale, height: imageHeigt * scale)
        
        let x = (bounds.size.width - clippingSize.width) / 2.0
        let y = (bounds.size.height - clippingSize.height) / 2.0
        
        
        return CGRect(origin: CGPoint(x: x, y: y), size: clippingSize)
    }
    
    
}
