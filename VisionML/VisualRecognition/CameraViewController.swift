import UIKit
import AVFoundation
import CoreML
import Vision

class CameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet var cameraView: UIView!
    @IBOutlet var tempImageView: UIImageView!
    
    @IBOutlet var captureButton: UIButton!
    @IBOutlet var retakeButton: UIButton!
    
    // Variables de la camara
    var captureSession: AVCaptureSession?
    var photoOutput: AVCapturePhotoOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    
    
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initCamera()
        
        self.retake()
        
    }
    
    func initCamera() {
        
        self.captureSession = AVCaptureSession()
        self.captureSession?.sessionPreset = .hd4K3840x2160
//        self.captureSession?.sessionPreset = .high
        
        
        
        
        let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        
        
        
        do {
            
            let input = try AVCaptureDeviceInput(device: backCamera!)
            self.captureSession?.addInput(input)
            
            self.photoOutput = AVCapturePhotoOutput()
            
            if (self.captureSession?.canAddOutput(self.photoOutput!) != nil) {
                self.captureSession?.addOutput(self.photoOutput!)
                self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
                self.previewLayer?.videoGravity = .resizeAspect
                self.previewLayer?.connection?.videoOrientation = .portrait
                self.cameraView.layer.addSublayer(self.previewLayer!)
                self.captureSession?.startRunning()
            }
            
        } catch {
            print("ERROR: \(error)")
        }
        
        self.previewLayer?.frame = self.view.bounds
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let error = error {
            print("ERROR: \(error)")
            return
        }
        
        let photoData = photo.fileDataRepresentation()!
        let dataProvider = CGDataProvider(data: photoData as CFData)!
        
        let cgImageRef = CGImage(jpegDataProviderSource: dataProvider, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
        
        
        self.classify(cgImageRef!) { (data) in
            self.push(data: data)
        }
        
        let image = UIImage(data: photoData)
        self.tempImageView.image = image
        self.tempImageView.isHidden = false
        
        
    }
    
    func classify(_ image: CGImage, completion: @escaping ([VNClassificationObservation]) -> Void) {
        DispatchQueue.global(qos: .background).async {
            guard let coreModel = try? VNCoreMLModel(for: Inceptionv3().model) else { return }
            
            let request = VNCoreMLRequest(model: coreModel, completionHandler: { (request, error) in
                guard var results = request.results as? [VNClassificationObservation] else { fatalError("Fallo al procesar datos") }
                
                results = results.filter({$0.confidence > 0.01})
                
                
                DispatchQueue.main.async {
                    completion(results)
                }
                
            })
            
            let handler = VNImageRequestHandler(cgImage: image, options: [:])
            
            do {
                try handler.perform([request])
            } catch {
                print("Error: \(error)")
            }
            
        }
        
    }
    
    
    func dismissResults() {
        getTableController { (tableController, drawer) in
            drawer.setDrawerPosition(position: .closed, animated: true)
            tableController.classifications = []
        }
    }
    
    func push(data: [VNClassificationObservation]) {
        getTableController { (tableController, drawer) in
            tableController.classifications = data
            self.dismiss(animated: true, completion: nil)
            drawer.setDrawerPosition(position: .partiallyRevealed, animated: true)
        }
        
    }
    
    func getTableController(run: (_ tableController: ResultsTableViewController, _ drawer: PulleyViewController) -> Void) {
        if let drawer = self.parent as? PulleyViewController {
            if let tableController = drawer.drawerContentViewController as? ResultsTableViewController {
                run(tableController, drawer)
                tableController.tableView.reloadData()
            }
        }
    }
    
    @IBAction func takePhoto() {
        self.photoOutput?.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
        self.captureButton.isHidden = true
        self.retakeButton.isHidden = false
        
        let alert = UIAlertController(title: "Procesando", message: "Por favor, espera...", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.startAnimating()
        
        alert.view.addSubview(activityIndicator)
        
        self.present(alert, animated: true)
        
        
    }
    
    @IBAction func retake() {
        self.tempImageView.isHidden = true
        self.captureButton.isHidden = false
        self.retakeButton.isHidden = true
        self.dismissResults()
    }
}
