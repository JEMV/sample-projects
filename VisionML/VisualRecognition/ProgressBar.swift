import UIKit

class ProgressBar: UIView {
    private var innerProgress: CGFloat = 0.0
    
    var progress: CGFloat {
        get {
            return self.innerProgress * self.bounds.width
        }
        set {
            if newValue > 1.0 {
                self.innerProgress = 1.0
            } else if newValue < 0.0 {
                self.innerProgress = 0.0
            } else {
                self.innerProgress = newValue
            }
            self.setNeedsDisplay()
        }
    }
    

    override func draw(_ rect: CGRect) {
        self.drawProgressBar(frame: bounds, progress: progress)
    }
    
    func drawProgressBar(frame: CGRect = CGRect(x: 0, y: 1, width: 288, height: 12), progress: CGFloat = 274.0) {
        
        let green = UIColor(red: 100/255, green: 221/255, blue: 23/255, alpha: 1.0)
        let yellow = UIColor(red: 255/255, green: 171/255, blue: 0/255, alpha: 1.0)
        let red = UIColor(red: 244/255, green: 67/255, blue: 54/255, alpha: 1.0)
        
        let progressPath = UIBezierPath(roundedRect: CGRect(x: frame.minX + 1, y: frame.minY + 1, width: frame.width - 2, height: frame.height - 2), cornerRadius: (frame.height - 2)/2)
        
        
        switch progress/self.bounds.width {
        case 0...0.66666:
            red.setStroke()
        case 0.66666...0.833333:
            yellow.setStroke()
        default:
            green.setStroke()
        }
        
        progressPath.lineWidth = 1
        progressPath.stroke()
        progressPath.addClip()
        
        let progressActivePath = UIBezierPath(roundedRect: CGRect(x: 1, y: 1, width: progress, height: frame.height - 2), cornerRadius: (frame.height - 2)/2)
        
        switch progress/self.bounds.width {
        case 0...0.66666:
            red.setFill()
        case 0.66666...0.833333:
            yellow.setFill()
        default:
            green.setFill()
        }
        
        progressActivePath.fill()
        
        
    }
    
}
