//
//  MasterViewController.swift
//  MECCE
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class CharacterListViewController: UITableViewController {

    var characterViewModels = [CharacterViewModel]()
    var filteredCharacterViewModels = [CharacterViewModel]()
    var searchController: UISearchController!
    
    private let characterCellIdentifier = "CharacterCell"


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        RequestManager.shared.loadCharacters { (characters) in
            if let characters = characters {
                self.characterViewModels = characters
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        self.setupViews()
    }
    
    private func setupViews() {
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search character"
        self.searchController.hidesNavigationBarDuringPresentation = false
        
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        if let title = Bundle.main.infoDictionary?["NavigationTitle"] as? String {
            self.navigationItem.title = title
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = true
        super.viewWillAppear(animated)
    }

    private func filterContentFor(searchText: String) {
        self.filteredCharacterViewModels = self.characterViewModels.filter({ (characterViewModel) -> Bool in
            return characterViewModel.name.contains(searchText) || characterViewModel.description.contains(searchText)
        })
    }
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: self.navigationItem.title, style: .plain, target: nil, action: nil)
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow, let navigation = segue.destination as? UINavigationController, let detailController = navigation.topViewController as? CharacterDetailsViewController {
                detailController.character = self.searchController.isActive ? self.filteredCharacterViewModels[indexPath.row] : characterViewModels[indexPath.row]
            }
        } else if segue.identifier == "showDetailiPhone" {
            if let indexPath = tableView.indexPathForSelectedRow, let detailController = segue.destination as? CharacterDetailsViewController {
                detailController.character = self.searchController.isActive ? self.filteredCharacterViewModels[indexPath.row] : characterViewModels[indexPath.row]
            }
        }
        self.searchController.isActive = false
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.isActive {
            return self.filteredCharacterViewModels.count
        } else {
            return self.characterViewModels.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.characterCellIdentifier, for: indexPath)

        let character = self.searchController.isActive ? self.filteredCharacterViewModels[indexPath.row] : self.characterViewModels[indexPath.row]
        cell.textLabel?.text = character.name
        return cell
    }
}

extension CharacterListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            self.filterContentFor(searchText: searchText)
            self.tableView.reloadData()
        }
    }
}


