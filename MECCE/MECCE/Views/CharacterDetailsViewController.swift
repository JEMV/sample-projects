//
//  DetailViewController.swift
//  MECCE
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class CharacterDetailsViewController: UIViewController {

    @IBOutlet weak var characterImageView: CachedImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var character: CharacterViewModel! {
        didSet {
            self.loadViewIfNeeded()
            self.navigationItem.title = self.character.name
            self.descriptionTextView.text = self.character.description
            self.characterImageView.loadImage(urlString: self.character.imageURL)
        }
    }
}

