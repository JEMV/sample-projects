//
//  RequestManager.swift
//  MECCE
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit


class RequestManager {
    
    static let shared = RequestManager()
    private let urlSession: URLSession
    let APIEndPoint: String = {
        return Bundle.main.infoDictionary?["DataURL"] as? String ?? ""
    }()
    
    private init() {
        self.urlSession = URLSession.shared
    }
    
    func loadCharacters(completion: @escaping (_ characters: [CharacterViewModel]?) -> Void) {
        guard let endPointURL = URL(string: self.APIEndPoint) else {
            completion(nil)
            return
        }
        
        self.urlSession.dataTask(with: endPointURL) { (data, response, error) in
            guard error == nil, let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data else {
                completion(nil)
                return
            }
            
            do {
                let characters = try JSONDecoder().decode(CharacterList.self, from: data)
                var characterList = [CharacterViewModel]()
                for character in characters.list {
                    characterList.append(CharacterViewModel(character: character))
                }
                completion(characterList)
                return
            } catch let jsonError {
                print("JSON Serialization error: \(jsonError)")
            }
        }.resume()
    }
    
}
