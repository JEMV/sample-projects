//
//  AppDelegate.swift
//  MECCE
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.window?.rootViewController = UIStoryboard(name: "iPhoneInterface", bundle: nil).instantiateInitialViewController()
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            guard let splitViewController = UIStoryboard(name: "iPadInterface", bundle: nil).instantiateInitialViewController() as? UISplitViewController else { return true }
            splitViewController.preferredDisplayMode = .allVisible
            splitViewController.delegate = self
            self.window?.rootViewController = splitViewController
        }
        self.window?.makeKeyAndVisible()
        return true
    }
    // MARK: - Split view

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? CharacterDetailsViewController else { return false }
        if topAsDetailController.character == nil {
            return true
        }
        return false
    }

}

