//
//  CharacterViewModel.swift
//  MECCE
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import Foundation

struct CharacterViewModel {
    
    let name: String
    let description: String
    let imageURL: String
    
    init(character: Character) {
        self.name = character.text.components(separatedBy: " - ").first ?? ""
        self.description = character.text.components(separatedBy: " - ").last ?? ""
        self.imageURL = character.icon.imageURL
    }
}
