//
//  Character.swift
//  Simpsons Character Viewer
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit


struct Character: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case text = "Text"
        case icon = "Icon"
    }
    
    let text: String
    let icon: Icon
}
