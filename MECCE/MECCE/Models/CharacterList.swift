//
//  CharacterList.swift
//  MECCE
//
//  Created by Jose Enrique Montañez Villanueva on 24/11/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

struct CharacterList: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case list = "RelatedTopics"
    }
    
    let list: [Character]
}
