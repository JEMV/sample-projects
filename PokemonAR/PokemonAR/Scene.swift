//
//  Scene.swift
//  PokemonAR
//
//  Created by Jose Enrique Montañez Villanueva on 1/3/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import SpriteKit
import ARKit
import GameplayKit

class Scene: SKScene {
    
    let remainingLabel = SKLabelNode()
    var timer: Timer?
    var targetsCreated = 0
    var targetCount = 0 {
        didSet {
            self.remainingLabel.text = "Faltan: \(self.targetCount)"
            if self.targetsCreated == 25 && self.targetCount == 0 {
                self.gameOver()
            }
        }
    }
    let startTime = Date()
    let deathSound = SKAction.playSoundFileNamed("QuickDeath", waitForCompletion: false)
    
    
    override func didMove(to view: SKView) {
        // Setup your scene here
        
        // Configuracion del HUD
        self.remainingLabel.fontSize = 30
        self.remainingLabel.fontName = "Avenir Next"
        self.remainingLabel.color = .white
        self.remainingLabel.position = CGPoint(x: 0, y: self.view!.frame.midY - 50)
        
        self.addChild(remainingLabel)
        
        self.targetCount = 0
        
        // Creacion de enemigos cada 3 segundos
        self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { (timer) in
            self.createTarget()
        })
        
    }
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //Localizar el primer toque del conjunto de toques
        //mirar si el toque cae dentro de nuestra vista de AR
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        print("El toque ha sido en: \(location.x), \(location.y)")
        
        
        //buscaremos todos los nodos que han sido tocados por ese toque de usuario
        let hit = self.nodes(at: location)
        
        
        
        //cogeremos el primer sprite del array que nos devuelve el metodo anterior (si lo hay) y animaremos ese pokemon hasta hacerlo desaparecer
        if let sprite = hit.first {
            
            let scaleOut = SKAction.scale(to: 2, duration: 0.4)
            let fadeOut = SKAction.fadeOut(withDuration: 0.4)
            let groupAction = SKAction.group([scaleOut, fadeOut, self.deathSound])
            let sequenceAction = SKAction.sequence([groupAction, SKAction.removeFromParent()])
            
            sprite.run(sequenceAction)
        }
        
        //actualizaremos que hay un poquemon menos con la variable targuet count
        self.targetCount -= 1
        
        
//        guard let sceneView = self.view as? ARSKView else {
//            return
//        }
//
//        // Create anchor using the camera's current position
//        if let currentFrame = sceneView.session.currentFrame {
//
//            // Create a transform with a translation of 0.2 meters in front of the camera
//            var translation = matrix_identity_float4x4
//            translation.columns.3.z = -0.2
//            translation.columns.3.y = 0.2
//
//
//
//
//            let transform = simd_mul(currentFrame.camera.transform, translation)
//
//
//
//            // Add a new anchor to the session
//            let anchor = ARAnchor(transform: transform)
//            sceneView.session.add(anchor: anchor)
//        }
    }
    
    func createTarget() {
        if targetsCreated == 25 {
            timer?.invalidate()
            timer = nil
            return
        }
        
        
        targetsCreated += 1
        targetCount += 1
        
        
        guard let sceneView = self.view as? ARSKView else { return }
        
        //1. Crear un generador de numeros aleatorios
        let random = GKRandomSource.sharedRandom()
        
        //2. crear una matriz de rotacion aleatoria en X
        let rotateX = float4x4(SCNMatrix4MakeRotation(2.0 * Float.pi * random.nextUniform(), 1, 0, 0))
        print("rotateX", rotateX)
        
        //3. crear una matriz de rotacion aleatoria en Y
        let rotateY = float4x4(SCNMatrix4MakeRotation(2.0 * Float.pi * random.nextUniform(), 0, 1, 0))
        print("rotateY", rotateY)
        
        //4. combinar las dos rotaciones con producto de matrices
        let rotation = simd_mul(rotateX, rotateY)
        print("rotation", rotation)
        
        //5. crear una traslacion de 1.5 metros en la direccion de la pantalla
        var translation = matrix_identity_float4x4
        translation.columns.3.z = -1.5
        print("translation", translation)
        
        //6. combinar la rotacion del paso 4 con la traslacion del paso 5
        let finalTransform = simd_mul(rotation, translation)
        print(finalTransform)
        
        //7. crear un punto de ancla en el punto final determinado en el paso 6
        let anchor = ARAnchor(transform: finalTransform)
        
        //8. añadir la ancla a la escena
        sceneView.session.add(anchor: anchor)
        
    }
    
    func gameOver() {
        //Ocultar la remainginLabel
        remainingLabel.removeFromParent()
        
        //Crear una nueva imagen con la foto de game over
        let gameOver = SKSpriteNode(imageNamed: "gameover")
        self.addChild(gameOver)
        
        //Calcular cuanto tiempo le ha llevado al usuario cazar todos los pokemon
        let timeTaken = Date().timeIntervalSince(self.startTime)
        
        //Mostrar ese tiempo que le ha llevado en pantalla en una etiqueta nueva
        let timeTakenLabel = SKLabelNode(text: "Te ha llevado: \(Int(timeTaken)) segundos")
        timeTakenLabel.fontSize = 40
        timeTakenLabel.color = .white
        timeTakenLabel.position = CGPoint(x: 0, y: -self.view!.frame.midY + 50)
        self.addChild(timeTakenLabel)
        
    }
}
