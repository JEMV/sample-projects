//
//  ViewController.swift
//  CarML
//
//  Created by Jose Enrique Montañez Villanueva on 12/28/18.
//  Copyright © 2018 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var modelSegmentedControl: UISegmentedControl!
    @IBOutlet weak var extrasSwitch: UISwitch!
    @IBOutlet weak var kmsLabel: UILabel!
    @IBOutlet weak var kmsSlider: UISlider!
    @IBOutlet weak var statusSegmentedControl: UISegmentedControl!
    @IBOutlet weak var priceLabel: UILabel!
    
    let cars = Cars()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.stackView.setCustomSpacing(25.0, after: self.modelSegmentedControl)
        self.stackView.setCustomSpacing(25.0, after: self.extrasSwitch)
        self.stackView.setCustomSpacing(25.0, after: self.kmsSlider)
        self.stackView.setCustomSpacing(50, after: self.statusSegmentedControl)
        self.calculateValue()
    }

    @IBAction func calculateValue() {
        
        // Formatear el valor del Slider
        let formater = NumberFormatter()
        formater.numberStyle = .decimal
        formater.maximumFractionDigits = 0
        
        let formattedKms = formater.string(for: self.kmsSlider.value) ?? "0"
        
        self.kmsLabel.text = "Kilometraje: \(formattedKms) kms"
        
        
        // Hacer el calculo del valor del auto con ML
        
        if let prediction = try? cars.prediction(modelo: Double(self.modelSegmentedControl.selectedSegmentIndex), extras: self.extrasSwitch.isOn ? 1.0 : 0.0, kilometraje: Double(self.kmsSlider.value), estado: Double(self.statusSegmentedControl.selectedSegmentIndex)) {
            
            let clampValue = max(500, prediction.precio)
            
            formater.numberStyle = .currency
            
            self.priceLabel.text = formater.string(for: clampValue)
            
        } else {
            self.priceLabel.text = "Error"
        }
        
        
        
    }
    
    
    
}

