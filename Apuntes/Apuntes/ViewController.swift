//
//  ViewController.swift
//  Apuntes
//
//  Created by Jose Enrique Montañez Villanueva on 4/12/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit
import PDFKit
import SafariServices

class ViewController: UIViewController, PDFViewDelegate, PDFDocumentDelegate {

    let pdfView = PDFView()
    
    let textView = UITextView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.pdfView.delegate = self
        
        // Configuracion del PDF View
        self.pdfView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(pdfView)
        
        self.pdfView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.pdfView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.pdfView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.pdfView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        
        // Configuracion del Text View
        self.textView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.textView)
        
        self.textView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.textView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.textView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.textView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.textView.isEditable = false
        self.textView.isHidden = true
        self.textView.textContainerInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        
        
        // Metodos de uso del pdf
        let search = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.promptForSearch))
        let share = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.shareSelection(sender:)))
        let previous = UIBarButtonItem(barButtonSystemItem: .rewind, target: self.pdfView, action: #selector(self.pdfView.goToPreviousPage(_:)))
        let next = UIBarButtonItem(barButtonSystemItem: .fastForward, target: self.pdfView, action: #selector(self.pdfView.goToNextPage(_:)))
        
        self.navigationItem.leftBarButtonItems = [search, share, previous, next]
        
        let segmentedControl = UISegmentedControl(items: ["PDF", "Solo texto"])
        segmentedControl.addTarget(self, action: #selector(self.changePDFMode(sender:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: segmentedControl)
        self.navigationItem.rightBarButtonItem?.width = 160
        
    }


    func load(_ name: String) {
        // Convertir el nombre del libro al nombre del archivo
        let fileName = name.replacingOccurrences(of: " ", with: "_").lowercased()
        
        // Buscar dentro del paquete de recursos el archivo con extension pdf
        guard let path = Bundle.main.url(forResource: fileName, withExtension: "pdf") else { return }
        
        // Cargar el pdf usando la clase PDFDocument, con una URL
        guard let pdfDocument = PDFDocument(url: path) else { return }
        
        // Asignar el PDFDocument a la PDFView de nuestra app
        pdfDocument.delegate = self
        self.pdfView.document = pdfDocument
        
        
        // Llamar al metodo goToFirstPage()
        self.pdfView.goToFirstPage(nil)
        
        self.readText()
        
        self.pdfView.autoScales = true
        
        // Mostrar el nombre del archivo en la barra de titulo del ipad
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.title = name
        }
    }
    
    @objc func promptForSearch() {
        let alert = UIAlertController(title: "Buscar", message: nil, preferredStyle: .alert)
        alert.addTextField()
        alert.addAction(UIAlertAction(title: "Buscar", style: .default, handler: { (action) in
            guard let searchText = alert.textFields?.first?.text else { return }
            
            guard let match = self.pdfView.document?.findString(searchText, fromSelection: self.pdfView.highlightedSelections?.first, withOptions: .caseInsensitive) else { return }
            
            self.pdfView.go(to: match)
            self.pdfView.highlightedSelections = [match]
            
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func shareSelection(sender: UIBarButtonItem) {
        guard let selection = self.pdfView.currentSelection?.attributedString else {
            let alert = UIAlertController(title: "No hay nada seleccionado", message: "Selecciona un fragmento del archivo para compartir", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return  }
        
        let activityVC = UIActivityViewController(activityItems: [selection], applicationActivities: nil)
        activityVC.popoverPresentationController?.barButtonItem = sender
        
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
    func pdfViewWillClick(onLink sender: PDFView, with url: URL) {
        
        let SFViewController = SFSafariViewController(url: url)
        SFViewController.modalPresentationStyle = .formSheet
        
        self.present(SFViewController, animated: true, completion: nil)
    }
    
    @objc func changePDFMode(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            // Mostrar PDF
            self.pdfView.isHidden = false
            self.textView.isHidden = true
        } else {
            // Mostrar TextView
            self.textView.isHidden = false
            self.pdfView.isHidden = true
            
        }
        
    }
    
    func readText() {
        guard let pageCount = self.pdfView.document?.pageCount else { return }
        
        let pdfContent = NSMutableAttributedString()
        
        let space = NSAttributedString(string: "\n\n\n")
        
        for i in 0..<pageCount {
            guard let page = self.pdfView.document?.page(at: i) else { continue }
            guard let pageContent = page.attributedString else { continue }
            
            pdfContent.append(space)
            pdfContent.append(pageContent)
            
        }
        
        let pattern = "https://[a-z0-9].[a-z]"
        let regexp = try? NSRegularExpression(pattern: pattern)
        let range = NSMakeRange(0, pdfContent.string.utf16.count)
        
        if let matches = regexp?.matches(in: pdfContent.string, options: [], range: range) {
            for match in matches.reversed() {
                pdfContent.replaceCharacters(in: match.range, with: "")
            }
        }
        
        self.textView.attributedText = pdfContent
    }
    
    func classForPage() -> AnyClass {
        return waterMark.self
    }
    
}

