//
//  waterMark.swift
//  Apuntes
//
//  Created by Jose Enrique Montañez Villanueva on 4/14/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit
import PDFKit

class waterMark: PDFPage {
    
    
    
    override func draw(with box: PDFDisplayBox, to context: CGContext) {
        // Dibuja la pagina actual del pdf
        super.draw(with: box, to: context)
        
        
        // Crear la marca de agua
        let stringText: NSString = "Capitulo de muestra\n del curso"
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor: UIColor.red, .font: UIFont.systemFont(ofSize: 30)]
        let stringSize = stringText.size(withAttributes: attributes)
        
        
        
        UIGraphicsPushContext(context)
        context.saveGState()
        
        // Donde dibujar la marca de agua
        let pageBounds = bounds(for: box)
        context.translateBy(x: (pageBounds.size.width-stringSize.width)/2, y: pageBounds.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
//        context.rotate(by: CGFloat(Double.pi/4.0))
        
        stringText.draw(at: CGPoint(x: 0, y: 100), withAttributes: attributes)
        
        context.restoreGState()
        UIGraphicsPopContext()
        
    }
    
    
}
