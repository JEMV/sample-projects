//
//  FilesTableViewController.swift
//  Apuntes
//
//  Created by Jose Enrique Montañez Villanueva on 4/12/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit

class FilesTableViewController: UITableViewController {

    let files = [
        "Matrices",
        "Vectores",
        "Espacios Vectoriales",
        "Aplicaciones Lineales",
        "Diagonalizacion",
        "Programacion Lineal",
        "Matematica Discreta",
        "Teoria de Grafos"
        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView(self.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.files.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fileCell", for: indexPath)

        cell.textLabel?.text = self.files[indexPath.row]
        
        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let navController = splitViewController?.viewControllers[1] as? UINavigationController else { return }
        
        guard let viewController = navController.viewControllers[0] as? ViewController else { return }
        
        viewController.load(self.files[indexPath.row])
    }

}
