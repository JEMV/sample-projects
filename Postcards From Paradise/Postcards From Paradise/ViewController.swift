//
//  ViewController.swift
//  Postcards From Paradise
//
//  Created by Jose Enrique Montañez Villanueva on 3/14/19.
//  Copyright © 2019 Jose Enrique Montañez Villanueva. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDragDelegate, UIDropInteractionDelegate, UIDragInteractionDelegate {
    
    
    
    
    
    @IBOutlet weak var postCardImageView: UIImageView!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    
    
    var colors = [UIColor]()
    
    var image: UIImage?
    
    var topText: NSString = "Bienvenido a iOS 11"
    var bottomText: NSString = "El mejor curso lanzado por Juan Gabriel hasta la fecha"
    
    var topFontName = "Avenir Next"
    var bottomFontName = "Avenir Next"
    
    var topFontColor = UIColor.white
    var bottomFontColor = UIColor.white
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.colors += [.black, .gray, .white, .red, .orange, .yellow, .green, .cyan, .blue, .purple, .magenta]
        
        for hue in 0...9 {
            for sat in 1...10 {
                let color = UIColor(hue: CGFloat(hue)/10.0, saturation: CGFloat(sat)/10.0, brightness: 1.0, alpha: 1.0)
                self.colors.append(color)
            }
        }
        
        let dropInteraction = UIDropInteraction(delegate: self)
        let dragInteraction = UIDragInteraction(delegate: self)
        
        self.postCardImageView.addInteraction(dropInteraction)
        self.postCardImageView.addInteraction(dragInteraction)
        
        
        
        self.renderPostCard()
        
    }
    
    // MARK: - CollectionView Data Source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCell", for: indexPath)
        
        let color = self.colors[indexPath.row]
        cell.backgroundColor = color
        cell.layer.borderWidth = 1
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func renderPostCard() {
        // 1. Definir la zona de dibujo para trabajar 3000x2400
        let drawRect = CGRect(x: 0, y: 0, width: 3000, height: 2400)
        
        // 2. Crear 2 rectangulos para los dos textos de la postal
        let topRect = CGRect(x: 300, y: 200, width: 2400, height: 800)
        let bottomRect = CGRect(x: 300, y: 1800, width: 2400, height: 600)
        
        // 3. A partir de los nombres de las fuentes, crear los dos objetos UIFont
        //    Dejaremos una fuente por defecto (la de sistema) por si algo falla
        let topFont = UIFont(name: self.topFontName, size: 250) ?? UIFont.systemFont(ofSize: 240)
        let bottomFont = UIFont(name: self.bottomFontName, size: 120) ?? UIFont.systemFont(ofSize: 80)
        
        // 4. NSMutableParagraphStyle para centrar el texto en la etiqueta
        let centered = NSMutableParagraphStyle()
        centered.alignment = .center
        
        // 5. Definir la estructura de la etiqueta como el color y la fuente (NSAttributedStringKey)
        let topAttributes: [NSAttributedString.Key: Any] =
            [
                NSAttributedString.Key.foregroundColor  : topFontColor,
                NSAttributedString.Key.font             : topFont,
                NSAttributedString.Key.paragraphStyle   : centered
            ]
        
        let bottomAttributes: [NSAttributedString.Key: Any] =
            [
                .foregroundColor    : bottomFontColor,
                .font               : bottomFont,
                .paragraphStyle     : centered
            ]
        
        // 6. Iniciar la renderizacion de la imagen (UIGraphicsImageRenderer)
        let renderer = UIGraphicsImageRenderer(size: drawRect.size)
        
        self.postCardImageView.image = renderer.image(actions: { (context) in
            
            // 6.1 Renderizar la zona con un fondo gris
            UIColor.lightGray.set()
            context.fill(drawRect)
            
            // 6.2 Pintaremos la imagen seleccionada del usuario (si hay alguna) empezando por el borde superior izquierdo
            self.image?.draw(at: CGPoint(x: 0, y: 0))
            
            // 6.3 Pintar las dos etiquetas de texto con los parametros configurados en 5
            self.topText.draw(in: topRect, withAttributes: topAttributes)
            self.bottomText.draw(in: bottomRect, withAttributes: bottomAttributes)
            
        })
    }
    
    
    // MARK: - UICollectionViewDragDelegate
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let color = self.colors[indexPath.row]
        let itemProvider = NSItemProvider(object: color)
        let item = UIDragItem(itemProvider: itemProvider)
        
        return [item]
    }
    
    // MARK: - UIDragInteractionDelegate
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        guard let image = self.postCardImageView.image else { return [] }
        let provider = NSItemProvider(object: image)
        let item = UIDragItem(itemProvider: provider)
        return [item]
    }
    
    // MARK: - UIDropInteractionDelegate
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        
        
        return UIDropProposal(operation: .copy)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        
        let dropLocation = session.location(in: self.postCardImageView)
        
        if session.hasItemsConforming(toTypeIdentifiers: [kUTTypePlainText as String]) {
            // Se ejecutará si lo que hemos soltado es un string
            session.loadObjects(ofClass: NSString.self) { (items) in
                guard let fontName = items.first as? String else { return }
                
                if dropLocation.y < self.postCardImageView.bounds.midY {
                    self.topFontName = fontName
                } else {
                    self.bottomFontName = fontName
                }
                
                self.renderPostCard()
                
            }
            
            
        } else if session.hasItemsConforming(toTypeIdentifiers: [kUTTypeImage as String]) {
            // Se ejecutará si lo que hemos soltado es una imagen
            session.loadObjects(ofClass: UIImage.self) { (items) in
                guard let image = items.first as? UIImage else { return }
                
                self.image = image
                self.renderPostCard()
            }
            
            
        } else {
            // Se ejecutará si lo que hemos soltado es un color
            session.loadObjects(ofClass: UIColor.self) { (items) in
                guard let color = items.first as? UIColor else { return }
                
                if dropLocation.y < self.postCardImageView.bounds.midY {
                    self.topFontColor = color
                } else {
                    self.bottomFontColor = color
                }

                self.renderPostCard()
            }
        }
    }
    
    
    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let originalSize = image.size
        
        let xRatio = targetSize.width / originalSize.width
        let yRatio = targetSize.height / originalSize.height
        
        let targetRatio = max(xRatio, yRatio)
        
        let newSize = CGSize(width: originalSize.width * targetRatio, height: originalSize.height * targetRatio)
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage!
        
    }
    
    // MARK: - Gesture Recognizer
    @IBAction func chageText(_ sender: UITapGestureRecognizer) {
        // 1. Encontrar la localizacion del tap dentro de la postal
        let tapLocation = sender.location(in: self.postCardImageView)
        
        // 2. Decidir si el usuario tiene que cambiar la etiqueta superior o inferior
        let changeTop = tapLocation.y < self.postCardImageView.bounds.midY ? true : false
        
        // 3. Crear un objeto UIAlertController con un textDield adicional para que el usuario escriba el texto
        let alertController = UIAlertController(title: "Cambiar texto", message: "Escribe el nuevo texto a mostrar", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.clearButtonMode = .whileEditing
            textField.placeholder = "Qué deseas mostrar aquí?"
            textField.text = changeTop ? self.topText as String : self.bottomText as String
            
        }
        
        
        // 4. Añadir accion 'Cambiar texto' que cambie el texto pertinentemente y llame al metodo renderPostcard()
        alertController.addAction(UIAlertAction(title: "Cambiar texto", style: .default, handler: { (_) in
            guard let newText = alertController.textFields?.first?.text else { return }
            
            if changeTop {
                self.topText = newText as NSString
            } else {
                self.bottomText = newText as NSString
            }
            
            self.renderPostCard()
            
        }))
        
        
        // 5. Añadir una opcion 'Cancelar' que pare el proceso
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        // 6. Mostrar la alert controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

